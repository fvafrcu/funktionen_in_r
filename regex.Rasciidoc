= Regular Expressions in **R**
Dominik Cullmann 
:toc2:
:toclevels: 5
:data-uri:
:duration: 60

== &Uuml;berblick
. Einleitung
. Werkzeuge
. R-Funktionen an Beispielen
. Fortgeschrittenes
. Anwendungen
. Ende

== Einleitung: https://en.wikipedia.org/wiki/Regular_expression[Regul&auml;re Ausdr&uuml;cke]

- Suchmuster und Mustererkennung (https://en.wikipedia.org/wiki/Stephen_Cole_Kleene[Stephen Cole Kleene])

- https://en.wikipedia.org/wiki/Comparison_of_regular_expression_engines

-  R (?base::regex)

    *     The TRE documentation at 
          http://laurikari.net/tre/documentation/regex-syntax/
    
    *     The POSIX 1003.2 standard at 
          http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html
   
    *     The ‘pcrepattern’ ‘man’ page (found as part of 
          http://www.pcre.org/original/pcre.txt), and details of Perl’s own
          implementation at http://perldoc.perl.org/perlre.html.


//begin.rcode, echo  = FALSE
is_probably_valid_email <- function(string) {
    pattern <- "^[a-zA-Z0-9_+.-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,3}$"
    is_valid <- grepl(pattern, string)
    return(is_valid)
}

is_valid_password <- function(string) {
    minimum_length <- 8
    is_long_enough <- nchar(string) >= minimum_length
    has_uppercase <- grepl("[[:upper:]]", string)
    has_lowercase <- grepl("[[:lower:]]", string)
    has_punctation <- grepl("[[:punct:]]", string)
    is_valid <- is_long_enough & has_uppercase & has_lowercase & 
        has_punctation
    return(is_valid)
}

//end.rcode

== Email und Passwort

== Email
//begin.rcode
mails <- c("dominik.cullmann@forst.bwlde", 
           "@2.de", 
           "dominik.cullmann@forst.bwl.de")

print(cbind(mails, is_probably_valid_email(mails)))
//end.rcode

==  Passwort
//begin.rcode
passwords <- c("Aa$4567",
               "aa$45678",
               "AA$45678",
               "Aa345678",
               "Aa$45678"
               )
print(cbind(passwords, is_valid_password(passwords)))
//end.rcode

== is_valid_password
//begin.rcode, eval  = FALSE
# min. 8 characters, min. 1 uppercase, min. 1 lowercase, min. 1 special character
is_valid_password <- function(string) {
    minimum_length <- 8
    is_long_enough <- nchar(string) >= minimum_length
    has_uppercase <- grepl("[[:upper:]]", string)
    has_lowercase <- grepl("[[:lower:]]", string)
    has_punctation <- grepl("[[:punct:]]", string)
    is_valid <- is_long_enough & has_uppercase & has_lowercase & 
        has_punctation
    return(is_valid)
}

//end.rcode

== is_probably_valid_email
//begin.rcode, eval  = FALSE
is_probably_valid_email <- function(string) {
    pattern <- "^[a-zA-Z0-9_+.-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,3}$"
    is_valid <- grepl(pattern, string)
    return(is_valid)
}
//end.rcode
 
== Werkzeuge
. Quantifier
. Groups
. Anchors
. Character Sets
. OR

== Quantifier 
. +{n,m}+
. +++
. +*+
. +?+

== Quantifier: +{n,m}+
Mindestens +n+ und h&ouml;chstens +m+ Wiederholungen.
//begin.rcode
replacement  <- "##"
x <- "B BA BAA BAAA CA"
gsub(pattern = "BA", replacement, x)
gsub(pattern = "BA{2,3}", replacement, x)
//end.rcode
== Quantifier: +?+
Eine oder keine Wiederholung.
//begin.rcode
gsub(pattern = "BA{0,1}", replacement, x)
gsub(pattern = "BA?", replacement, x)
//end.rcode
== Quantifier: +++  
Mindestens eine Wiederholung.
//begin.rcode
gsub(pattern = "BA{1,}", replacement, x)
gsub(pattern = "BA+", replacement, x)
//end.rcode
== Quantifier: +*+
Mindestens keine Wiederholung.
//begin.rcode
gsub(pattern = "BA{0,}", replacement, x)
gsub(pattern = "BA*", replacement, x)
//end.rcode

== Groups
//begin.rcode
replacement  <- "##"
x <- "ABA ABABA ABABABA ABABABABA"
gsub(pattern = "(BA)", replacement, x)
gsub(pattern = "(BA){2,3}", replacement, x)
//end.rcode
== Anchors
//begin.rcode
replacement  <- "##"
x <- "Ha Ha HaHa Ha"
gsub(pattern = "^Ha", replacement, x)
gsub(pattern = "Ha$", replacement, x)
gsub(pattern = "\\<Ha\\>", replacement, x)
//end.rcode

== Character Sets

. Eigene Definitionen
. Vorgefertigte Definitionen
. Der Punkt

//begin.rcode
x <-  paste(c(letters, LETTERS," ", pi), collapse = "")
print(x)
replacement  <- "#"
//end.rcode
== Character Sets: Eigene Definitionen
//begin.rcode
gsub(pattern = "[A-Z]", replacement, x)
gsub(pattern = "[1-3]", replacement, x)
gsub(pattern = "[^1-3]", replacement, x) # Negative Set
//end.rcode

== Character Sets: Vorgefertigte Definitionen
//begin.rcode
gsub(pattern = "[[:upper:]]", replacement, x) # Depends on current locale! [A-Z] does not!
gsub(pattern = "[[:lower:]]", replacement, x)
gsub(pattern = "[[:upper:][:lower:]]", replacement, x) # composite set
gsub(pattern = "[[:alpha:]]", replacement, x) # pre-defined, same as above
//end.rcode

== Character Sets: Der Punkt
//begin.rcode
replacement  <- "##"
gsub(pattern = "1.", replacement, x)
sub(pattern = "1.*", replacement, x)
sub(pattern = ".*", replacement, x)
//end.rcode

== OR
//begin.rcode
mail <- readLines(file.path(".", "src", "mail.txt"))
grep(pattern = "^From:", mail, value = TRUE)
grep(pattern = "^Subject:", mail, value = TRUE)
grep(pattern = "^[FS][[:alnum:]]*\\>:", mail, value = TRUE)
grep(pattern = "^(From|Subject):", mail, value = TRUE)
//end.rcode



== R-Funktionen an Beispielen
Zwei Beispielstrings, die Funktionen

- Kernfunktionen
. base::regexpr
. base::regexec
. base::gregexpr
- Anwendungsfunktionen
. base::grep
. base::grepl
. base::sub
. base::gsub


== A Simple String
//begin.rcode
string <- "This is a (character) string."
paste(length(string), nchar(string), sep = " ## ")
pattern <- "is"
//end.rcode

== A Simple String: Kernfunktionen
//begin.rcode
regexpr(pattern, string) # a vector giving start and length of the first match
regexec(pattern, string) # a list giving start and length of first match and groupings
gregexpr(pattern, string) # a vector giving start and length of matches
//end.rcode
== A Simple String: Anwendungsfunktionen
//begin.rcode
grep(pattern, string) # indices of elements of "string" matching "pattern"
grep(pattern, string, value = TRUE) # the elements of "string" matching "pattern"
grepl(pattern, string) # Do elements of "string" contain "pattern"?
sub(pattern, "##", string) # replace first match of pattern in string
gsub(pattern, "##", string) # replace all matches of pattern in string
//end.rcode

== A Character Vector
//begin.rcode
string <- c("This is a (not too) long sentence, stored in a (character) string.",
            "Well, it is (actually) a vector of class character.", 
            "And this is not a single sentence.")

paste(length(string), nchar(string), sep = " ## ")
# match a word, possibly surrouned by punctuation, followed by some sort of
# space and a word starting with either th or st.
pattern <- "[[:punct:]]?\\<[[:alnum:]]*\\>[[:punct:]]?\\s\\<(th|st)[[:alnum:]]*\\>" 
# \\s is the space class, see ?regex
//end.rcode

== A Character Vector: Kernfunktionen
//begin.rcode
regexpr(pattern, string) # a vector giving start and length of the first match
regexec(pattern, string) # a list giving start and length of first match and groupings
gregexpr(pattern, string) # a vector giving start and length of matches
//end.rcode

== A Character Vector: Anwendungsfunktionen
//begin.rcode
grep(pattern, string) # indices of elements of "string" matching "pattern"
grep(pattern, string, value = TRUE) # the elements of "string" matching "pattern"
grepl(pattern, string) # Do elements of "string" contain "pattern"?
sub(pattern, "##", string) # replace first match of pattern in string
gsub(pattern, "##", string) # replace all matches of pattern in string
//end.rcode


== Fortgeschrittenes
. Gefr&auml;&szlig;igkeit
. R&uuml;ckverweise


== Gefr&auml;&szlig;igkeit (greediness)
//begin.rcode
print(string)
substitution <- "" 
pattern <- " \\(.*\\)" # greedy quantifier
print(new_string <- gsub(pattern, substitution, string))

pattern <- " \\([^\\)]*\\)" # non-greedy quantifier using negation
print(new_string <- gsub(pattern, substitution, string))

pattern <- " \\(.*?\\)" # non-greedy quantifier -- wtf?
print(new_string <- gsub(pattern, substitution, string))

//end.rcode

== R&uuml;ckverweise

//begin.rcode
pattern <- "(\\<a\\>)"
substitution <- "\\1[INJECTED TEXT]"
gsub(pattern, substitution, string)

pattern <- "^([[:punct:]]?\\<[[:alnum:]_]*\\>[[:punct:]]?[[:space:]]?)\\<[[:alnum:]_]*\\>"
substitution <- "\\1[OVERWRITTEN]"
sub(pattern, substitution, string)


# inner grouping without backreference -- n+1th word, substitution stays constant
pattern <- "^((?:[[:punct:]]?\\<[[:alnum:]_]*\\>[[:punct:]]?[[:space:]]?[[:punct:]]?){3})\\<[[:alnum:]_]*\\>"
substitution <- "\\1[OVERWRITTEN]"
sub(pattern, substitution, string)

pattern <- "^((?:[[:punct:]]?\\<[[:alnum:]_]*\\>[[:punct:]]?[[:space:]]?[[:punct:]]?){7})\\<[[:alnum:]_]*\\>"
sub(pattern, substitution, string)

# matching the rest of the line into a second group
pattern <- paste0(pattern, "(.*)$")
substitution <- paste0(substitution, "\\2")
sub(pattern, substitution, string)

//end.rcode

== Anwendungen
. Dateipfade
. camelCase

== Dateipfade I
//begin.rcode
#% Anwendung mit Dateipfaden
##% Dateien bereitstellen
rm(list= ls())
path <- file.path(tempdir(), "foo")
unlink(path, recursive = TRUE)
dir.create(path)
for (name in c("mtcars", "iris")) {
    write.csv(get(name), file = file.path(path,  paste0(name, ".csv")))
    write.table(get(name), file = file.path(path, paste0(name, ".txt")))
}
print(list.files(path, full.names = TRUE))
//end.rcode

== Dateipfade II
//begin.rcode
##%  _Nur_ CSV-Dateien lesen und wieder schreiben als deutsches CSV
# FIXME: Ich will eine Funktion sein!
csv_files <- list.files(path, pattern = "^.*\\.csv$", full.names = TRUE)
for (file in csv_files) {
    file_name <- basename(file)
    new_file <- sub("(.*)(\\.csv)", "\\1_german\\2", file)
    write.csv2(read.csv(file), file = new_file)
} 
print(files <- list.files(path, full.names = TRUE))
//end.rcode

== Dateipfade III
//begin.rcode
# i-te Zeile testweise lesen
i <- 4
print(rbind(readLines(grep("iris.csv", files, value = TRUE))[i],
            readLines(grep("iris_german.csv", files, value = TRUE))[i]))
//end.rcode

== camelCase I
//begin.rcode
code_file <- file.path(tempdir(), "code.R")
code <- "fileRename <- function(from, to) {
rootDirectory <- dirname(to)
if (! dir.exists(rootDirectory)) dir.create(rootDirectory, recursive = TRUE)
return(file.rename(from, to))
}
"

cat(code, file = code_file)
print(readLines(code_file))
//end.rcode

== camelCase II
//begin.rcode
code <- readLines(code_file)
warning("This will blow calls to foreign camelCase such as utils::sessionInfo()!")
code <- gsub("([A-Z][a-z])", "_\\L\\1", code, perl = TRUE)
writeLines(code, code_file)
print(readLines(code_file))
//end.rcode

== camelCase III
//begin.rcode

eval(parse(text = code))
from <- list.files(file.path(tempdir(), "foo"), pattern = "^.*iris.txt$", 
                   full.names = TRUE)
to <- file.path(tempdir(), "some_dir", basename(from))
file_rename(from = from, to = to)
list.files(file.path(tempdir(), "some_dir"), full.names = TRUE)
head(read.table(to))

//end.rcode

== Ende 
. Regex in R
. regmatches
. Tsch&uuml;ss

== Regex in R

Meine Lieblinge

- base::regex
- base::grep # (grepl, sub, gsub, regexec, regexpr, gregexpr)
- base::list.files
- base::ls 

Sonst noch:

- install.packages(c("stringi")) # (ICU regex engine)
- utils::apropos # (find)
- utils::browseEnv
- utils::glob2rx
- utils::help.search
- base::strsplit
- base::regmatches

== Relevanz in meinen packages
//begin.rcode
count_function_calls <- function(path) {
    files <- grep("\\.Rcheck/", invert = TRUE, value = TRUE, 
                  list.files(path, recursive = TRUE, pattern = ".*[rR]$", 
                             full.names = TRUE)
                  )

    content <- NULL
    for (input in files) {
        if (!exists("input")) input <- files[192]
        content <- c(content, try(readLines(input)))

    }
    loc <- grep("^\\s*#", content, invert = TRUE, value = TRUE)
    loc <- gsub("(\\<(if|while|until|switch|ifelse|function)\\s?\\()", "#(", loc)
    code <-  paste(loc, collapse = "")
    split_char <- "@"
    code <- gsub("@", "former_split_char", code)
    calls <- gsub("((?:[[:alpha:]]+\\:{2,3})?[$A-Za-z._]+\\()", "@\\1", code)
    calls <- unlist(strsplit(calls, split = "@", fixed = TRUE))
    calls <- calls[-1]
    calls <- sub("\\(.*$", "", calls)
    total <- length(calls)
    counts <- summary(as.factor(calls), maxsum = total)
    ranked <- data.frame(count = counts, relative = counts / total, 
                         rank = as.integer(length(counts) - rank(counts) + 1))
    ranked[["relative_rank"]] <- ranked[["rank"]] / nrow(ranked)
    ordered <- ranked[order(ranked[, "relative"], decreasing = TRUE), TRUE]
    return(ordered)
}

counts <- count_function_calls("~/git/cs/fvafrcu")
head(counts, n = 10)
counts[grep("^(g?sub|grepl?|g?regexpr|regexec)$", row.names(counts)), TRUE]
//end.rcode



== regmatches
Es gibt noch base::regmatches(), aber das benutze ich nie, das ist mir zu kompliziert.
Beispiel aus der Hilfe:
//begin.rcode
 ## Consider
 x <- "John (fishing, hunting), Paul (hiking, biking)"
 ## Suppose we want to split at the comma (plus spaces) between the
 ## persons, but not at the commas in the parenthesized hobby lists.
 ## One idea is to "blank out" the parenthesized parts to match the
 ## parts to be used for splitting, and extract the persons as the
 ## non-matched parts.
 ## First, match the parenthesized hobby lists.
 m <- gregexpr("\\([^)]*\\)", x)
 ## Write a little utility for creating blank strings with given numbers
 ## of characters.
 blanks <- function(n) strrep(" ", n)
 ## Create a copy of x with the parenthesized parts blanked out.
 s <- x
 regmatches(s, m) <- Map(blanks, lapply(regmatches(s, m), nchar))
 ## Compute the positions of the split matches (note that we cannot call
 ## strsplit() on x with match data from s).
 m <- gregexpr(", *", s)
 ## And finally extract the non-matched parts.
 regmatches(x, m, invert = TRUE)

//end.rcode

== Tsch&uuml;ss

- Regex sind m&auml;chtig: _is_probably_valid_email_ wird mit anderen Zeichenkettenfunktionen eine sehr lange Funktion, die miserabel zu warten ist, falls sich die Definition dessen, was als Email akzeptiert werden soll, &auml;ndert. Mit regex:
//begin.rcode
    pattern <- "^[a-zA-Z0-9_+.-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,3}$"
//end.rcode
- (vor allem f&auml;lschlicherweise gefr&auml;&szlig;ige) regex sind _sehr_ fehleranf&auml;llig. + 
  _Teste daher alle Deine regex sorgf&auml;ltig, besonders, wenn sie ".", "*" oder "+" beinhalten._

