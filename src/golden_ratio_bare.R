golden_ratio  <- function() {
    phi <- (1 + sqrt(5)) / 2
    return(phi)
}
