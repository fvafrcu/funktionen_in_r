coldr.html: coldr.Rasciidoc
	Rscript -e 'rasciidoc::render("coldr.Rasciidoc", asciidoc_args = "-b slidy")'
regex.html: regex.Rasciidoc
	Rscript -e 'rasciidoc::render("regex.Rasciidoc", asciidoc_args = "-b slidy")'
unit_testing.html: unit_testing.Rasciidoc
	Rscript -e 'rasciidoc::render("unit_testing.Rasciidoc",  asciidoc_args = "-b slidy")'
index_slides.html: index.Rasciidoc
	Rscript -e 'rasciidoc::render("index.Rasciidoc", what = "slides", clean = TRUE, asciidoc_args = "-b slidy")'
index.html: index.Rasciidoc
	Rscript -e 'rasciidoc::render("index.Rasciidoc")'
